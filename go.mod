module codeberg.org/codeberg/pages

go 1.16

require (
	github.com/OrlovEvgeny/go-mcache v0.0.0-20200121124330-1a8195b34f3a
	github.com/akrylysov/pogreb v0.10.1
	github.com/go-acme/lego/v4 v4.5.3
	github.com/reugn/equalizer v0.0.0-20210216135016-a959c509d7ad
	github.com/rs/zerolog v1.26.0
	github.com/stretchr/testify v1.7.0
	github.com/urfave/cli/v2 v2.3.0
	github.com/valyala/fasthttp v1.31.0
	github.com/valyala/fastjson v1.6.3
)
